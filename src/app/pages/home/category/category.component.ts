import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Category } from 'src/app/models/category';
import { Localization } from 'src/app/models/location';

import SwiperCore from "swiper/core";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnChanges {

  @Input() categories: Array<Category>;

  constructor() { }

  ngOnChanges(): void {
     console.log(this.categories)
  }

  ngOnInit(): void { }

  onSwiper(swiper) {
    console.log(swiper)
  }

  onSlideChange() {
    console.log('slide change')
  }

}
