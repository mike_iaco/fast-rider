import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  signupForm: FormGroup;

  constructor(private _builder: FormBuilder) {
    this.signupForm = this._builder.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      clave: ['', Validators.required],
    });
  }

  onSubmit(values) {
    console.log(values);
  }
  ngOnInit(): void {}
}
