import { TestBed } from '@angular/core/testing';

import { CityServiceProxy } from './city.service';

describe('CityService', () => {
  let service: CityServiceProxy;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CityServiceProxy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
