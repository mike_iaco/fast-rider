import { environment } from './../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Account } from 'src/app/models/account';

@Injectable({
  providedIn: 'root'
})
export class AccountServiceProxy { 

  constructor(private http: HttpClient) {

  }

  create (account: Account) {
    let url = environment.baseUrl + "/accounts";
    let options = {
        headers: new HttpHeaders({
        "Content-Type": "application/json",
        "Accept":  "application/json"
      }),
      body: account
    };

    return this.http.request('post', url, options);
  }

}

