import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/models/category';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoryServiceProxy {

  constructor(private http: HttpClient) { }

  get(cityId: string | undefined): Observable<Category[]> {
    let url = environment.baseUrl + "/categories?";
    if(cityId !== undefined){
      url += 'cityId=' + encodeURIComponent(cityId) + '&'
    }
    url = url.replace(/[?&]$/, '');

    let options = {
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    }
    return this.http.request<Category[]>('get', url, options);
  }
}
