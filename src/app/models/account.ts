export interface IAccount {
  nombre: string;
  ciudad: string;
  email: string;
  clave: string;
}

export class Account implements IAccount {
  id: number;
  nombre: string;
  ciudad: string;
  email: string;
  clave: string;
}
