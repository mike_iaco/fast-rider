
export interface ICategory {
  id: number | undefined;
  name: string | undefined;
  description: string | undefined;
  imageUrl: string | undefined;
}

export class Category implements ICategory {
  id: number | undefined;
  name: string | undefined;
  description: string | undefined;
  imageUrl: string | undefined;

  constructor(data?: ICategory) {
    this.init(data);
  }

  init(data?: any) {
    if (data) {
        this.name = data["name"];
        this.description = data["description"];
        this.imageUrl = data["imageUrl"];
        this.id = data["id"];
    }
  }

  static fromJS(data: any): Category {
    data = typeof data === 'object' ? data : {};
    let result = new Category();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["id"] = this.id;
      data["name"] = this.name;
      data["description"] = this.description;
      data["imageUrl"] =  this.imageUrl;
      return data;
  }

  clone(): Category {
      const json = this.toJSON();
      let result = new Category();
      result.init(json);
      return result;
  }
}
