
export interface ICity {
  id: string | undefined;
  name: string | undefined;
}

export class City implements ICity {
  id: string | undefined;
  name: string | undefined;

  constructor(data?: ICity) {
    this.init(data);
  }

  init(data?: any) {
    if (data) {
        this.name = data["name"];
        this.id = data["id"];
    }
  }

  static fromJS(data: any): City {
    data = typeof data === 'object' ? data : {};
    let result = new City();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
      data = typeof data === 'object' ? data : {};
      data["id"] = this.id;
      data["name"] = this.name;
      return data;
  }

  clone(): City {
      const json = this.toJSON();
      let result = new City();
      result.init(json);
      return result;
  }
}
