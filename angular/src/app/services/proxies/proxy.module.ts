import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CityServiceProxy } from './city.service';
import { HttpClientModule } from '@angular/common/http';
import { CategoryServiceProxy } from './category.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    CityServiceProxy,
    CategoryServiceProxy
  ]
})
export class ProxyModule { }
