import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { City } from 'src/app/models/city';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CityServiceProxy {

  constructor(private http: HttpClient) { }

  getAll(): Observable<City[]> {
    let url = environment.baseUrl + "/cities";
    let options = {
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    }
    return this.http.request<City[]>('get', url, options);
  }


  get(id: string | undefined): Observable<City> {
    let url = environment.baseUrl + "/cities?";
    if(id !== undefined){
      url += 'id=' + encodeURIComponent(id) + '&'
    }
    url = url.replace(/[?&]$/, '');

    let options = {
      headers: new HttpHeaders({
        "Accept": "application/json"
      })
    }
    return this.http.request<City>('get', url, options);
  }
}
