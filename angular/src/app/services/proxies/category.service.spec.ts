import { TestBed } from '@angular/core/testing';

import { CategoryServiceProxy } from './category.service';

describe('CategoryService', () => {
  let service: CategoryServiceProxy;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CategoryServiceProxy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
