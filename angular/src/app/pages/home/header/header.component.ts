import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { City } from 'src/app/models/city';
import { CityServiceProxy } from 'src/app/services/proxies/city.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public cities: Array<City> = [];
  public selectedCity: any;

  @Output()
  public cityChange: EventEmitter<City> = new EventEmitter();

  constructor(private cityService: CityServiceProxy) { }

  ngOnInit(): void {
    this.cityService
    .getAll()
    .subscribe((cities: any) => {
      this.cities = cities;
      if(this.cities.length > 0) {
        this.selectedCity = this.cities[0];
        this.cityChange.emit(this.selectedCity);
      }
    });
  }

  public onChange(event: any) {
    this.cityChange.emit(this.selectedCity);
  }

}
