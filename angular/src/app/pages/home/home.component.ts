import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import { City } from 'src/app/models/city';
import { Localization } from 'src/app/models/location';
import { CategoryServiceProxy } from 'src/app/services/proxies/category.service';
import { SwiperOptions } from 'swiper/types/swiper-options';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public categories: Array<Category>;

  constructor(private categoryService: CategoryServiceProxy ) { }

  ngOnInit(): void { }

  onCityChange(city: City | undefined) {
    if(city !== undefined) {
      this.categoryService.get(city.id)
        .subscribe((categories: any) => {
          this.categories = categories;
        });
    }
  }
}
