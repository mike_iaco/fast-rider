import { HomeComponent } from './home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwiperModule } from 'swiper/angular';

import { HomeRoutingModule } from './home-routing.module';
import { HeaderComponent } from './header/header.component';
import { CategoryComponent } from './category/category.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent,
    CategoryComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule,
    SwiperModule,
    NgbModule
  ]
})
export class HomeModule { }
