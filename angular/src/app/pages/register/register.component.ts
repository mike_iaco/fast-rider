import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AccountServiceProxy } from './../../services/proxies/account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {  signupForm: FormGroup;

  success: boolean = false;

  constructor(private _builder: FormBuilder, private accountService: AccountServiceProxy) {
   
  }

  ngOnInit(): void {
    this.signupForm = this._builder.group({
      nombre: ['', Validators.required],
      ciudad: ['', Validators.required],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      clave: ['', Validators.required],
    });
  }
  
  onSubmit(values) {
    const account = this.signupForm.value;
    this.accountService.create(account).subscribe();
    this.success = true;
  }
}
