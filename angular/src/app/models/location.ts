import { Category } from "./category";

export interface ILocalization {
  cityId: string;
  categories: Category[];
}

export class Localization {
  cityId: string;
  categories: Category[];

  constructor(data?: ILocalization) {
    this.init(data);
  }

  init(data?: any) {
    if (data) {
        this.cityId = data["cityId"];
        this.categories = data["categories"];
    }
  }
}
